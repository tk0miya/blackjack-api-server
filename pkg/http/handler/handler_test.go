package handler

import (
	"net/http/httptest"
	"strings"
	"testing"
)

func TestPlayer_validate(t *testing.T) {
	type fields struct {
		Name           string
		WebhookBaseURL string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name:    "valid",
			fields:  fields{"Taro", "http://localhost/webhook"},
			wantErr: false,
		},
		{
			name:    "player_name: empty",
			fields:  fields{"", "http://localhost/webhook"},
			wantErr: true,
		},
		{
			name:    "webhook_baseurl: empty",
			fields:  fields{"Taro", ""},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Player{
				Name:           tt.fields.Name,
				WebhookBaseURL: tt.fields.WebhookBaseURL,
			}
			if err := p.validate(); (err != nil) != tt.wantErr {
				t.Errorf("Player.validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestGame_POST(t *testing.T) {
	tests := []struct {
		name    string
		content string
		code    int
	}{
		{
			name:    "valid",
			content: `{"player_name": "Taro", "webhook_baseurl": "http://localhost/"}`,
			code:    204,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Reset()
			content := strings.NewReader(tt.content)
			w := httptest.NewRecorder()
			r := httptest.NewRequest("POST", "/game", content)
			Game(w, r)
			if w.Code != tt.code {
				t.Errorf("Entry() code = %d, expected %d", w.Code, tt.code)
			}
		})
	}
}
