package handler

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
)

type Player struct {
	Name           string `json:"player_name"`
	WebhookBaseURL string `json:"webhook_baseurl"`
}

func (p *Player) validate() error {
	if p.Name == "" {
		return errors.New("name is required")
	}

	if p.WebhookBaseURL == "" {
		return errors.New("webhook_baseurl is required")
	}

	return nil
}

var players []Player

func Game(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		var player Player
		json.NewDecoder(r.Body).Decode(&player)
		if err := player.validate(); err != nil {
			w.WriteHeader(400)
			return
		}

		AddPlayer(player)
		log.Printf("player %s joined\n", player.Name)
		w.WriteHeader(204)
	default:
		w.WriteHeader(400)
	}
}

func Reset() {
	players = nil
}

func AddPlayer(player Player) {
	players = append(players, player)
}

func GetPlayers() []Player {
	return players
}
