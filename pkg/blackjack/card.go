package blackjack

import (
	"errors"
	"math/rand"
	"strconv"
	"time"
)

type CardSuit int

const (
	Spade CardSuit = iota + 1
	Heart
	Diamond
	Club
)

func (suit CardSuit) String() string {
	switch suit {
	case Spade:
		return "Spade"
	case Heart:
		return "Heart"
	case Diamond:
		return "Diamond"
	case Club:
		return "Club"
	default:
		return "Other"
	}
}

type CardNumber int

func (number CardNumber) String() string {
	switch number {
	case 1:
		return "A"
	case 11:
		return "J"
	case 12:
		return "Q"
	case 13:
		return "K"
	default:
		return strconv.Itoa(int(number))
	}
}

type Card struct {
	Suit   CardSuit
	Number CardNumber
}

type Deck struct {
	Cards []Card
}

func NewDeck() *Deck {
	deck := new(Deck)

	for suit := Spade; suit <= Club; suit++ {
		for number := 1; number < 14; number++ {
			deck.Cards = append(deck.Cards, Card{suit, CardNumber(number)})
		}
	}

	return deck
}

func (deck *Deck) Shuffle() {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(deck.Cards), func(i, j int) { deck.Cards[i], deck.Cards[j] = deck.Cards[j], deck.Cards[i] })
}

func (deck *Deck) Draw() (Card, error) {
	if len(deck.Cards) == 0 {
		return Card{}, errors.New("no cards in deck")
	}

	card := deck.Cards[0]
	deck.Cards = deck.Cards[1:]
	return card, nil
}
