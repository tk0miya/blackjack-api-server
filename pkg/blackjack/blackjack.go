package blackjack

import "errors"

const BLACKJACK = 21

type Player struct {
	Name   string
	Cards  []Card
	Bust   bool
	Played bool
}

func (p Player) Point() int {
	ace := 0
	point := 0
	for _, card := range p.Cards {
		point += card.point()
		if card.Number == 1 {
			ace += 1
		}
	}

	for i := 0; i < ace; i++ {
		if point > 21 {
			point -= 10
		}
	}

	return point
}

func (c Card) point() int {
	switch c.Number {
	case 1:
		return 11
	case 11:
		return 10
	case 12:
		return 10
	case 13:
		return 10
	default:
		return int(c.Number)
	}
}

type Session struct {
	Cards   Deck
	Players []Player
}

func NewSession() *Session {
	session := new(Session)
	session.Cards = *NewDeck()
	session.Cards.Shuffle()
	return session
}

func (session *Session) Join(name string) {
	player := Player{}
	player.Name = name
	session.Players = append(session.Players, player)
}

func (session *Session) Deal() {
	for i := 0; i < len(session.Players); i++ {
		for j := 0; j < 2; j++ {
			card, _ := session.Cards.Draw()
			session.Players[i].Cards = append(session.Players[i].Cards, card)
		}
	}
}

func (session *Session) Hit(i int) error {
	if len(session.Players) < i {
		return errors.New("out of index")
	}

	player := &session.Players[i]
	if player.Point() >= BLACKJACK {
		return errors.New("can't draw any cards")
	}

	card, err := session.Cards.Draw()
	if err != nil {
		return err
	}

	player.Cards = append(player.Cards, card)

	if player.Point() > BLACKJACK {
		player.Bust = true
	}

	return nil
}

func (session *Session) Stand(i int) error {
	if len(session.Players) < i {
		return errors.New("out of index")
	}

	player := &session.Players[i]
	player.Played = true

	return nil
}
