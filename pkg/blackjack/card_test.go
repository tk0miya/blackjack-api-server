package blackjack

import (
	"testing"
)

func TestNewDeck(t *testing.T) {
	deck := NewDeck()
	if len(deck.Cards) != 52 {
		t.Errorf("NewDeck().cards = %d, want 52", len(deck.Cards))
	}
}

func TestDraw(t *testing.T) {
	deck := NewDeck()
	card := deck.Cards[0]
	got, err := deck.Draw()
	if err != nil {
		t.Errorf("deck.Draw() error = %v, wantErr nil", err)
		return
	}
	if got != card {
		t.Errorf("deck.Draw() = %v, want %v", got, card)
	}
	if len(deck.Cards) != 51 {
		t.Errorf("NewDeck().Cards = %d, want 51", len(deck.Cards))
	}

	deck.Cards = make([]Card, 0)
	_, err = deck.Draw()
	if err == nil {
		t.Errorf("deck.Draw() error = %v, wantErr err", err)
		return
	}
}
