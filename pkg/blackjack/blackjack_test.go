package blackjack

import (
	"testing"
)

func TestPlayer_Point(t *testing.T) {
	tests := []struct {
		name  string
		cards []Card
		want  int
	}{
		{
			name:  "Blackjack (King)",
			cards: []Card{{Spade, 1}, {Spade, 13}},
			want:  21,
		},
		{
			name:  "Blackjack (10)",
			cards: []Card{{Spade, 1}, {Spade, 10}},
			want:  21,
		},
		{
			name:  "Two aces",
			cards: []Card{{Spade, 1}, {Heart, 1}},
			want:  12,
		},
		{
			name:  "Ace, Jack and Queen",
			cards: []Card{{Spade, 1}, {Spade, 11}, {Spade, 12}},
			want:  21,
		},
		{
			name:  "Number cards",
			cards: []Card{{Spade, 2}, {Spade, 3}, {Spade, 4}},
			want:  9,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Player{
				Cards: tt.cards,
			}
			if got := p.Point(); got != tt.want {
				t.Errorf("Player.Point() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSession(t *testing.T) {
	session := NewSession()
	if len(session.Cards.Cards) != 52 {
		t.Errorf("Session.Cards = %v, want 52", session.Cards)
	}

	session.Join("Taro")
	session.Join("Jiro")

	if len(session.Players) != 2 {
		t.Errorf("Session.Players = %v, want 2", session.Players)
	}

	session.Deal()
	if len(session.Cards.Cards) != 48 {
		t.Errorf("Session.Cards = %v, want 48", session.Cards)
	}
	for i, player := range session.Players {
		if len(player.Cards) != 2 {
			t.Errorf("Session.Players[%d].Cards = %v, want 2", i, player.Cards)
		}
	}

	// Hit 可能な場合
	session.Players[0].Cards = []Card{{Spade, 1}, {Spade, 2}}
	err := session.Hit(0)
	if err != nil {
		t.Errorf("Session.Hit() = %v, want nil", err)
	}
	if len(session.Cards.Cards) != 47 {
		t.Errorf("Session.Cards = %v, want 47", session.Cards)
	}
	if len(session.Players[0].Cards) != 3 {
		t.Errorf("Session.Players[0].Cards = %v, want 3", session.Players[0].Cards)
	}

	// Hit 不可の場合
	session.Players[0].Cards = []Card{{Spade, 1}, {Spade, 10}}
	err = session.Hit(0)
	if err == nil {
		t.Errorf("Session.Hit() = %v, want err", err)
	}
	if len(session.Cards.Cards) != 47 {
		t.Errorf("Session.Cards = %v, want 47", session.Cards)
	}
	if len(session.Players[0].Cards) != 2 {
		t.Errorf("Session.Players[0].Cards = %v, want 2", session.Players[0].Cards)
	}

	// Hit して 21 を超えた場合 (bust)
	session.Players[0].Cards = []Card{{Spade, 10}, {Spade, 11}}
	session.Cards.Cards[0] = Card{Spade, 2} // 次のカードをスペードの 2に差し替える
	err = session.Hit(0)
	if err != nil {
		t.Errorf("Session.Hit() = %v, want err", err)
	}
	if len(session.Players[0].Cards) != 3 {
		t.Errorf("Session.Players[0].Cards = %v, want 2", session.Players[0].Cards)
	}
	if session.Players[0].Point() != 22 {
		t.Errorf("Session.Players[0].Point() = %v, want 22", session.Players[0].Point())
	}
	if session.Players[0].Bust != true {
		t.Errorf("Session.Players[0].Bust = %v, want true", session.Players[0].Bust)
	}

	// Stand
	session.Players[0].Cards = []Card{{Spade, 1}, {Spade, 2}}
	err = session.Stand(0)
	if err != nil {
		t.Errorf("Session.Stand() = %v, want nil", err)
	}
	if session.Players[0].Played != true {
		t.Errorf("Session.Players[0].Played = %v, want true", session.Players[0].Played)
	}
}
