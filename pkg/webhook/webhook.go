package webhook

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/tk0miya/blackjack-api-server/pkg/blackjack"
)

type RequestBody struct {
	MyCard  []string `json:"my_cards"`
	Players []Player `json:"players"`
}

type Player struct {
	Name   string   `json:"name"`
	Cards  []string `json:"cards"`
	Played bool     `json:"played"`
	Bust   bool     `json:"bust"`
}

func Do(baseurl string, session blackjack.Session, target int) (string, error) {
	request_body := RequestBody{}
	for i, player := range session.Players {
		if i == target {
			for _, card := range player.Cards {
				request_body.MyCard = append(request_body.MyCard, card.Number.String())
			}
		} else {
			cards := []string{}
			for j, card := range player.Cards {
				if j == 0 {
					cards = append(cards, card.Number.String())
				} else {
					cards = append(cards, "*")
				}
			}

			p := Player{player.Name, cards, player.Played, player.Bust}
			request_body.Players = append(request_body.Players, p)
		}
	}

	content, err := json.Marshal(request_body)
	if err != nil {
		return "", err
	}

	request, err := http.NewRequest("POST", baseurl+"/cards", bytes.NewBuffer(content))
	if err != nil {
		return "", err
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	result, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	return string(result), nil
}
