package webhook

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/tk0miya/blackjack-api-server/pkg/blackjack"
)

func TestDo(t *testing.T) {
	session := blackjack.NewSession()
	session.Join("Taro")
	session.Join("Jiro")
	session.Players[0].Cards = []blackjack.Card{{Suit: blackjack.Spade, Number: 1}, {Suit: blackjack.Spade, Number: 10}}
	session.Players[0].Played = true
	session.Players[0].Bust = true
	session.Players[1].Cards = []blackjack.Card{{Suit: blackjack.Spade, Number: 8}, {Suit: blackjack.Spade, Number: 9}}

	type args struct {
		session blackjack.Session
		target  int
	}
	tests := []struct {
		name        string
		args        args
		response    string
		wantRequest string
		wantErr     bool
	}{
		{
			name:        "valid",
			args:        args{*session, 0},
			response:    "Hit",
			wantRequest: `{"my_cards":["A","10"],"players":[{"name":"Jiro","cards":["8","*"],"played":false,"bust":false}]}`,
			wantErr:     false,
		},
		{
			name:        "valid",
			args:        args{*session, 1},
			response:    "Stand",
			wantRequest: `{"my_cards":["8","9"],"players":[{"name":"Taro","cards":["A","*"],"played":true,"bust":true}]}`,
			wantErr:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				content, _ := ioutil.ReadAll(r.Body)
				if string(content) != tt.wantRequest {
					t.Errorf("Do() request = %v, want %v", string(content), tt.wantRequest)
				}

				io.WriteString(w, tt.response)
			})
			ts := httptest.NewServer(h)
			defer ts.Close()

			result, err := Do(ts.URL, tt.args.session, tt.args.target)
			if (err != nil) != tt.wantErr {
				t.Errorf("Do() error = %v, wantErr %v", err, tt.wantErr)
			}
			if result != tt.response {
				t.Errorf("Do() result = %v, want %v", err, tt.response)
			}
		})
	}
}
