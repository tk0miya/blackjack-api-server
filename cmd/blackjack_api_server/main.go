package main

import (
	"log"
	"net/http"
	"time"

	"gitlab.com/tk0miya/blackjack-api-server/pkg/blackjack"
	"gitlab.com/tk0miya/blackjack-api-server/pkg/http/handler"
	"gitlab.com/tk0miya/blackjack-api-server/pkg/webhook"
)

func main() {
	ticker := time.NewTicker(30 * time.Second)
	done := make(chan bool)

	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				Play()
				handler.Reset()
			}
		}
	}()

	http.HandleFunc("/game", handler.Game)
	http.ListenAndServe(":8080", nil)
}

func Play() {
	players := handler.GetPlayers()
	if len(players) < 2 {
		log.Printf("No game.")
		return
	}

	session := blackjack.NewSession()
	for _, player := range players {
		session.Join(player.Name)
	}
	session.Deal()

	for i, player := range players {
		hit_or_stand, _ := webhook.Do(player.WebhookBaseURL, *session, i)
		switch hit_or_stand {
		case "hit":
			session.Hit(i)
			continue
		case "stand":
			session.Stand(i)
		default:
			log.Printf("Unknown command. Ingored: %v", hit_or_stand)
		}
	}

	for _, player := range session.Players {
		log.Printf("Point of %s: %d", player.Name, player.Point())
	}
}
