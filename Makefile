.PHONY: all
all: blackjack-api-server.macos blackjack-api-server.linux blackjack-api-server.exe

.PHONY: clean
clean:
	rm -f blackjack-api-server.macos blackjack-api-server.linux blackjack-api-server.exe

.PHONY: blackjack-api-server.macos
blackjack-api-server.macos:
	GOOS=darwin go build -o blackjack-api-server.macos cmd/blackjack_api_server/main.go

.PHONY: blackjack-api-server.linux
blackjack-api-server.linux:
	GOOS=linux go build -o blackjack-api-server.linux cmd/blackjack_api_server/main.go

.PHONY: blackjack-api-server.exe
blackjack-api-server.exe:
	GOOS=windows GOARCH=amd64 go build -o blackjack-api-server.exe cmd/blackjack_api_server/main.go
